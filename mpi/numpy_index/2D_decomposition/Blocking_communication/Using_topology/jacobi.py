# 
# Jacobi routine for CFD calculation
#
'''
Jacobi function
Computes the steam function.
Parametres: niter   - number of iterations
            psi     - array of initial values with dimensions [pm][n]
'''
import numpy as np
from mpi4py import MPI

def jacobi(niter, psi):

    (pm, pn) = psi.shape
    pm = pm - 2
    pn = pn - 2
      
    # MPI Initialization
    size    = MPI.COMM_WORLD.Get_size()    # number of processes in the communicator 

    # non-periodic cartesian topology, rank numbers are not reordered, dims = (rows, cols)
    cart          = MPI.COMM_WORLD.Create_cart(dims=(size, 1), periods=(False, False), reorder=False) 
    source1, down  = cart.Shift(direction=0, disp=-1)   # displacement to down 
    source2, right = cart.Shift(direction=1, disp=1)    # displacement to right   

    tmp = np.zeros((pm+2, pn+2), dtype=np.float)
    # tags for messages 
    tag0 = 0    # 0 - send to down rank/recv from up one
    tag1 = 1    # 1 - send to up rank/recv from down one
    tag2 = 2    # 2 - send to right rank/recv from left one
    tag3 = 3    # 3 - send to left rank/recv from right one
    for iter in range(niter):
        # Changing halos
        # Up - Down
        MPI.COMM_WORLD.Send([psi[pm][:], MPI.FLOAT], dest = down,     tag = tag0)
        MPI.COMM_WORLD.Recv([psi[0 ][:], MPI.FLOAT], source = source1, tag = tag0) 
      
        MPI.COMM_WORLD.Send([psi[1 ][:],   MPI.FLOAT], dest = source1, tag = tag1) 
        MPI.COMM_WORLD.Recv([psi[pm+1][:], MPI.FLOAT], source = down, tag = tag1) 

        # Right - Left
        MPI.COMM_WORLD.Send([psi[:][pn], MPI.FLOAT], dest = right,    tag = tag2)
        MPI.COMM_WORLD.Recv([psi[:][0 ], MPI.FLOAT], source = source2, tag = tag2) 
      
        MPI.COMM_WORLD.Send([psi[:][1   ], MPI.FLOAT], dest = source2,  tag = tag3) 
        MPI.COMM_WORLD.Recv([psi[:][pn+1], MPI.FLOAT], source = right, tag = tag3) 


        tmp[1:pm+1,1:pn+1] = 0.25 * (psi[2:pm+2,1:pn+1]+psi[0:pm,1:pn+1]+psi[1:pm+1,2:pn+2]+psi[1:pm+1,0:pn])

        # Update psi
        np.copyto(psi[1:pm+1,1:pn+1], tmp[1:pm+1,1:pn+1])
