#!/usr/bin/env python
#
# CFD Calculation (MPI)
# =====================
#
# Simulation of inviscid flow in a 2D box using the Jacobi algorithm.
#
# Python version using blocking MPI - uses numpy and loop, no topologies
# 2D decomposition
# 
# EPCC, 2014
# Modified: 01/08/2016
# Marta Cudova, screetech@gmail.com
# =========================================================================== #

import sys
import time

# Import numpy
import numpy as np
np.set_printoptions(threshold=np.inf)

# Import the local "util.py" methods
import util

# Import MPI - calls MPI_Init() and MPI_Finalize() is registered. Calling this 
# functions generates MPI Error.
from mpi4py import MPI

# Import the external jacobi function from "jacobi.py"
from jacobi import jacobi

def main(argv):

    # Test we have the correct number of arguments
    if len(argv) < 2:
        print("Usage: cfd.py <scalefactor> <iterations>")
        sys.exit(1)
    
    # Get the systen parameters from the arguments
    scalefactor = int(sys.argv[1])
    niter = int(sys.argv[2])
       
    # MPI Initialization
    my_rank = MPI.COMM_WORLD.Get_rank()    # my rank in the communicator
    size    = MPI.COMM_WORLD.Get_size()    # number of processes in the communicator 
    
    if my_rank == 0:
        sys.stdout.write("\n2D CFD Simulation\n")
        sys.stdout.write("=================\n")
        sys.stdout.write("Scale Factor = {0}\n".format(scalefactor))
        sys.stdout.write("  Iterations = {0}\n".format(niter))
        # Time the initialisation
        tstart = time.time()
   

    # Set the minimum size parameters
    mbase = 32
    nbase = 32
    bbase = 10
    hbase = 15
    wbase =  5

    # Set the dimensions of the array
    m = mbase*scalefactor
    n = nbase*scalefactor
    
    # check the consistency between te number of ranks and dimension sizes
    if m%size != 0 or n%size != 0:    
        print("Oops! Wrong number of ranks. Dimension sizes: " + str(m) + ", " + str(n) + " Try it again!")
        MPI.COMM_WORLD.Abort(1)
        sys.exit(1)
        
    # Set the parameters for boundary conditions
    b = bbase*scalefactor 
    h = hbase*scalefactor
    w = wbase*scalefactor
           
    # Calculate local size
    # indexes for processes (redimension) - each process works on its own slice
    # important: Python >= 3 use // for int deviding, and / for float deviding
    mp = m//size
    pn = n//size 
    
    # non-periodic cartesian topology, rank numbers are not reordered, dims = (rows, cols)
    cart          = MPI.COMM_WORLD.Create_cart(dims=(size, 1), periods=(False, False), reorder=False) 
    source, down  = cart.Shift(direction=0, disp=-1)   # displacement to down 
    source, right = cart.Shift(direction=1, disp=1)    # displacement to right      
        
    
    # Define the psi array of dimension [mp+2][np+2] and set it to zero
    psi = np.zeros((mp+2, pn+2), dtype=np.float)
    # python is row-major in default
    # indexes for boundaries
    istart = mp * my_rank
    istop  = istart + mp  
    
    # Set the boundary conditions on bottom edge
    # Shared edge - divide values among down-situated processes
    for i in range(b+1, b+w):
        if i >= istart and i <= istop:  # rank filter
            psi[i-istart][0] = float(i-b)
    for i in range(b+w, m+1):
        if i >= istart and i <= istop:        
            psi[i-istart][0] = float(w)

    jstart = pn * my_rank
    jstop  = jstart + pn
    # Set the boundary conditions on right edge
    for j in range(1, h+1):
        if j >= jstart and j <= jstop:
            psi[mp+1][j-jstart] = float(w)
    for j in range(h+1, h+w):
        if j >= jstart and j <= jstop:
            psi[mp+1][j-jstart] = float(w-j+h)
    
    if my_rank == 0:
        # Write the simulation details
        tend = time.time()
        sys.stdout.write("\nInitialisation took {0:.5f}s\n".format(tend-tstart))
        sys.stdout.write("\nGrid size = {0} x {1}\n".format(m, n))
        # Call the Jacobi iterative loop (and calculate timings)
        sys.stdout.write("\nStarting main Jacobi loop...\n")
    
    MPI.COMM_WORLD.Barrier()    
    if my_rank == 0:
        tstart = MPI.Wtime()
    # ---------- JACOBI ---------- #     
    jacobi(niter, psi)
    # ---------------------------- #
    MPI.COMM_WORLD.Barrier()
    if my_rank == 0:    
        tend = MPI.Wtime() 
        sys.stdout.write("...finished\n")
        sys.stdout.write("\nCalculation took {0:.5f}s\n\n".format(tend-tstart))
       
    psi_new = np.zeros((m, n+2), dtype=np.float)
    prefix = np.zeros((1, n+2), dtype=np.float)
    suffix = np.zeros((1, n+2), dtype=np.float)    
    if my_rank == 0:
        prefix = psi[0]
        MPI.COMM_WORLD.Recv([suffix, MPI.FLOAT], source = size-1, tag = 0) 
    elif my_rank == size-1:
        suffix = psi[mp+1]
        MPI.COMM_WORLD.Send([suffix, MPI.FLOAT], dest = 0,   tag = 0)
       
    psi = psi[1:mp+1][:]
        
    MPI.COMM_WORLD.Gather([psi, MPI.FLOAT], [psi_new, MPI.FLOAT], root=0)
    if  my_rank == 0:    
        psi_new = np.vstack((prefix, psi_new))
        psi_new = np.vstack((psi_new, suffix))
    
    # Write the output file
    if my_rank == 0:     
        util.write_data(m, n, scalefactor, psi_new, "velocity.dat", "colourmap.dat")
        # Write the output file
        plot_data(psi_new, "flow.png")

    # Finish nicely
    sys.exit(0)

# Create a plot of the data using matplotlib
def plot_data(psi, outfile):

    # Get the inner dimensions
    (m, n) = psi.shape
    m = m - 2
    n = n - 2

    # Define and zero the velocity arryas
    umod = np.zeros((m, n))
    ux = np.zeros((m, n))
    uy = np.zeros((m, n))

    for i in range(1, m+1):
        for j in range(1, n+1):
            # Compute velocities and magnitude squared
            ux[i-1,j-1] =  (psi[i,j+1] - psi[i,j-1])/2.0
            uy[i-1,j-1] = -(psi[i+1,j] - psi[i-1,j])/2.0
            mvs = ux[i-1,j-1]**2 + uy[i-1,j-1]**2
            # Scale the magnitude
            umod[i-1,j-1] = mvs**0.5
 
    # Plot a heatmap overlayed with velocity streams
    import matplotlib

    # Plot to image file without need for X server
    # This needs to occur before "import pyplot"
    matplotlib.rcParams['font.size'] = 8
    matplotlib.use("Agg")

    # Import the required functions
    from matplotlib import pyplot as plt
    from matplotlib import cm

    fig = plt.figure()

    # Regular grids
    x = np.linspace(0, m-1, m)
    y = np.linspace(0, n-1, n)

    # We want the image to appear in natural (x,y) coordinates,
    # rather than a "matrix". This actually means we must
    # transpose x and y *and* use the "origin" argument in
    # imshow() to turn the whole thing upside down.

    ux = np.transpose(ux)
    uy = np.transpose(uy)
    umod = np.transpose(umod)

    # Line widths are scaled by modulus of velocity
    lw = 3 * umod/umod.max()

    # Create the stream lines denoting the velocities
    plt.streamplot(x, y, ux, uy, color='k', density=1.5, linewidth=lw)

    # Create the heatmap denoting the modulus of the velocities

    plt.imshow(umod, origin = 'lower', interpolation='nearest', cmap=cm.jet)

    # Save the figure to the output PNG file
    fig.savefig(outfile)

# Function to create tidy way to have main method
if __name__ == "__main__":
        main(sys.argv[1:])
