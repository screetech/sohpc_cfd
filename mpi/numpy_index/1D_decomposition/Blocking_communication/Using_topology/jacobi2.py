# 
# Jacobi routine for CFD calculation
#
'''
Jacobi function
Computes the steam function.
Parametres: niter   - number of iterations
            psi     - array of initial values with dimensions [pm][n]
'''
import numpy as np
from mpi4py import MPI

def jacobi(niter, psi):

    (mp, n) = psi.shape
    mp = mp - 2
    n = n - 2
    
    # neighbourhood of the process
    up   = 0
    down = 1
    neighbourhood = [0, 0]  
    
    # MPI Initialization      
    size    = MPI.COMM_WORLD.Get_size()    # number of processes in the communicator 
    # non-periodic cartesian topology, rank numbers are not reordered, dims = (rows, cols)
    cart    = MPI.COMM_WORLD.Create_cart(dims=(size, 1), periods=(False, False), reorder=True) 
    neighbourhood[down], neighbourhood[up] = cart.Shift(direction=0, disp=1)    # displacement to right (up)
    tmp = np.zeros((mp+2, n+2), dtype=np.float)
    # tags for messages 
    tag0 = 0    # 0 - send to next rank/recv from previou one
    tag1 = 1    # 1 - send to previos rank/recv from next one
 
    for iter in range(niter):
        # Changing halos
        MPI.COMM_WORLD.Send([psi[mp][:], MPI.FLOAT], dest   = neighbourhood[up],   tag = tag0)
        MPI.COMM_WORLD.Recv([psi[0 ][:], MPI.FLOAT], source = neighbourhood[down], tag = tag0) 

        MPI.COMM_WORLD.Send([psi[1 ][:],   MPI.FLOAT], dest   = neighbourhood[down], tag = tag1) 
        MPI.COMM_WORLD.Recv([psi[mp+1][:], MPI.FLOAT], source = neighbourhood[up],   tag = tag1) 

        tmp[1:mp+1,1:n+1] = 0.25 * (psi[2:mp+2,1:n+1]+psi[0:mp,1:n+1]+psi[1:mp+1,2:n+2]+psi[1:mp+1,0:n])

        # Update psi
        np.copyto(psi[1:mp+1,1:n+1], tmp[1:mp+1,1:n+1])
