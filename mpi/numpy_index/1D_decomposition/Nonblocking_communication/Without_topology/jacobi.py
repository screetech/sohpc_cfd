# 
# Jacobi routine for CFD calculation
#
'''
Jacobi function
Computes the steam function.
Parametres: niter   - number of iterations
            psi     - array of initial values with dimensions [pm][n]
            size    - number of processes working on the problem
'''
import numpy as np
from mpi4py import MPI

def jacobi(niter, psi):

    (mp, n) = psi.shape
    mp = mp - 2
    n = n - 2
       
    # MPI Initialization
    my_rank = MPI.COMM_WORLD.Get_rank()    # my rank in the communicator
    size    = MPI.COMM_WORLD.Get_size()    # number of processes in the communicator 
    status  = [MPI.Status(), MPI.Status(), MPI.Status(), MPI.Status()] # get MPI status object
    
    next_rank = my_rank + 1
    prev_rank = my_rank - 1
    
    # Boundaries of the grid
    if next_rank >= size:
        next_rank = MPI.PROC_NULL
    if prev_rank < 0:
        prev_rank = MPI.PROC_NULL

    tmp = np.zeros((mp+2, n+2), dtype=np.float)
    tag0 = 0
    tag1 = 1

    for iter in range(niter):
        # Changing halos
        req0 = MPI.COMM_WORLD.Isend([psi[mp][:],   MPI.FLOAT], dest = next_rank,   tag = tag0) 
        req2 = MPI.COMM_WORLD.Isend([psi[1 ][:],   MPI.FLOAT], dest = prev_rank,   tag = tag1) 

        req3 = MPI.COMM_WORLD.Irecv([psi[mp+1][:], MPI.FLOAT], source = next_rank, tag = tag1) 
        req1 = MPI.COMM_WORLD.Irecv([psi[0   ][:], MPI.FLOAT], source = prev_rank, tag = tag0)          
        
        MPI.Request.Waitall([req0, req1, req2, req3], status)

        # Use index notation and offsets to compute the stream function
        tmp[1:mp+1,1:n+1] = 0.25 * (psi[2:mp+2,1:n+1]+psi[0:mp,1:n+1]+psi[1:mp+1,2:n+2]+psi[1:mp+1,0:n])

        # Update psi
        np.copyto(psi[1:mp+1,1:n+1], tmp[1:mp+1,1:n+1])
