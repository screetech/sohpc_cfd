# -*- coding: utf-8 -*-
''' 
# Jacobi function for CFD calculation
# -----------------------------------
# Basic Python version using lists
#
# Changed stopping criterion - error correction 
# (not exact number of iterations).
# Modified: 21/07/2016
# Marta Cudova, screetech@gmail.com '''
# =========================================================================== #

import sys

'''
Input: 
* err - stopping criterion
* psi
'''
#@profile
def jacobi(err, psi):

    # Get the inner dimensions
    m = len(psi) - 2
    n = len(psi[0]) -2

    # Define the temporary array and zero it
    tmp = [[0 for col in range(n+2)] for row in range(m+2)]
    
    isSatisfied = 0    # not satisfied
    niter       = 1    # counter of iterations
    # Iterate for number of iterations
    #for iter in range(1,niter+1):
    while not isSatisfied:
        prevSatisf  = 1    # set to satisfied
        # Loop over the elements computing the stream function
        for i in range(1,m+1):
            for j in range(1,n+1):
                tmp[i][j] = 0.25 * (psi[i+1][j]+psi[i-1][j]+psi[i][j+1]+psi[i][j-1])
                if niter%10 == 0: # check it each niter iteration
                    change = tmp[i][j] - psi[i][j]
                    if change >= err:
                        prevSatisf = 0 # if only one element is not satisfied, isSatisfied is still false
                        #print(change)
                    isSatisfied = prevSatisf            
        
        # Update psi
        for i in range(1,m+1):
            for j in range(1,n+1):
                psi[i][j] = tmp[i][j]

        # Debug output
        if niter%1000 == 0:
            sys.stdout.write("completed iteration {0}\n".format(niter))
        # Incrementation of niter
        niter = niter + 1
        
    sys.stdout.write("Number of Iterations: {0}\n".format(niter))