#!/usr/bin/env python
#
# CFD Calculation (MPI)
# =====================
#
# Simulation of inviscid flow in a 2D box using the Jacobi algorithm.
#
# Basic Python version using MPI - uses lists, no communication between processes
# -> the result will not be exactly the same
# 
# EPCC, 2014
# Modified: 22/07/2016
# Marta Cudova, screetech@gmail.com
# =========================================================================== #
import sys
import time

# Import the local "util.py" methods
import util

# Import MPI - calls MPI_Init() and MPI_Finalize() is registered. Calling this 
# functions generates MPI Error.
from mpi4py import MPI

# Import the external jacobi function from "jacobi.py"
from jacobi import jacobi

#@profile
def main(argv):

    # Test we have the correct number of arguments
    if len(argv) < 2:
        print("Usage: cfd.py <scalefactor> <iterations>")
        sys.exit(1)
    
    # Get the systen parameters from the arguments
    scalefactor = int(argv[0])
    niter = int(argv[1])
    
    sys.stdout.write("\n2D CFD Simulation\n")
    sys.stdout.write("=================\n")
    sys.stdout.write("Scale factor = {0}\n".format(scalefactor))
    sys.stdout.write("Iterations   = {0}\n".format(niter))

    # MPI Initialization
    #comm    = MPI.COMM_WORLD
    my_rank = MPI.COMM_WORLD.Get_rank()    # my rank in the communicator
    size    = MPI.COMM_WORLD.Get_size()    # number of processes in the communicator 
    status  = MPI.Status()       # get MPI status object
    
    
    # Time the initialisation
    tstart = time.time()
    
    # Set the minimum size parameters
    mbase = 32
    nbase = 32
    bbase = 10
    hbase = 15
    wbase =  5

    # Set the dimensions of the array
    m = mbase*scalefactor
    n = nbase*scalefactor
    
    # Set the parameters for boundary conditions
    b = bbase*scalefactor 
    h = hbase*scalefactor
    w = wbase*scalefactor 
    
    # Calculate local size
    # indexes for processes (redimension) - each process works on its own slice
    # important: Python >= 3 use // for int deviding, and / for float deviding
    mp = m//size
    # np = n -> use n!
    
    # Write the simulation details
    if my_rank == 0:
        sys.stdout.write("\nGrid size = {0} x {1}\n".format(m, n))
    
    # Define the psi array of dimension [mp+2][n+2] and set it to zero
    psi = [[0 for col in range(n+2)] for row in range(mp+2)]
    
    # python is row-major in default
    # indexes for boundaries
    istart = mp * my_rank
    istop  = istart + mp - 1    
    #print("Hi, I am ", my_rank, " from ", size, " my istart is ", istart, " and istop ", istop)
    
    # Set the boundary conditions on bottom edge
    # Shared edge - divide values among processes
    for i in range(b+1, b+w):
        if i >= istart and i <= istop:  # rank filter
            #print("rank ", my_rank," size ", size, " b+1 ", b+1, " b+w ", b+w, " istart ", istart, " i ", i, " psi dim ", len(psi), len(psi[0]))
            psi[i-istart+1][0] = float(i-b)
    for i in range(b+w, m+1):
        if i >= istart and i <= istop:        
            psi[i-istart+1][0] = float(w)

    # Set the boundary conditions on right edge
    # This slice is owned by one (last) process - because of row-majority
    if my_rank == size-1:
        for j in range(1, h+1):
            psi[mp+1][j] = float(w)
        for j in range(h+1, h+w):
            psi[mp+1][j] = float(w-j+h)
    
    # Call the Jacobi iterative loop (and calculate timings)
    if my_rank == 0:    
        sys.stdout.write("\nStarting main Jacobi loop ...\n\n")
    
    MPI.COMM_WORLD.Barrier()
    if my_rank == 0:
        tstart = MPI.Wtime()
    jacobi(niter, psi, MPI.COMM_WORLD)
    MPI.COMM_WORLD.Barrier()
    if my_rank == 0:    
        tend = MPI.Wtime()

    if my_rank == 0:
        sys.stdout.write("\n... finished\n")
        sys.stdout.write("\nCalculation took {0:.5f}s\n\n".format(tend-tstart))
    
    # Write the output files for subsequent visualisation
    util.write_data(mp, n, scalefactor, psi, "velocity.dat", "colourmap.dat")

    MPI.Finalize()
    # Finish nicely
    sys.exit(0)

# Function to create tidy way to have main method
if __name__ == "__main__":
        main(sys.argv[1:])
