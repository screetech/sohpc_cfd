# 
# Jacobi function for CFD calculation (MPI)
#
# Basic Python version using MPI and lists
#
# Modified: 22/07/2016
# Marta Cudova, screetech@gmail.com
# =========================================================================== #

import sys

'''
Jacobi function
Computes the steam function.
Parametres: niter   - number of iterations
            psi     - array of initial values with dimensions [pm][n]
            size    - number of processes working on the problem
'''
from mpi4py import MPI
#@profile
def jacobi(niter, psi, comm):

    # Get the inner dimensions
    m   = len(psi)    - 2   # number of rows
    n   = len(psi[0]) - 2   # number of columns
   
   # MPI Initialization
    #comm    = MPI.COMM_WORLD
    my_rank = comm.Get_rank()    # my rank in the communicator
    size    = comm.Get_size()    # number of processes in the communicator 
    status  = MPI.Status()       # get MPI status object
    pm  = m//size# number of rows per process
    my_rank = comm.Get_rank()
    
    next_rank = my_rank + 1
    prev_rank = my_rank - 1
    
    if next_rank >= size:
        next_rank = MPI.COMM_NULL
    if prev_rank < 0:
        prev_rank = MPI.COMM_NULL

    # Define the temporary array and zero it
    #tmp = [[0 for col in range(n+2)] for row in range(pm+2)]

    # Iterate for number of iterations
    for iter in range(1,niter+1):
        # blocking communication
        # buffer is a tuple [data, count, type]
        #comm.Sendrecv([psi[pm][1], MPI.FLOAT], dest = next,   sendtag = 0, [psi[0][1],  MPI.FLOAT], source = prev, recvtag = 0, status = status)
        comm.Send([psi[pm:][1:], n, MPI.FLOAT], dest = next_rank, tag = 0)
        comm.Recv([psi[0:][1:], n, MPI.FLOAT], source = prev_rank, tag = 0)
        #comm.Sendrecv([psi[1][1],    MPI.FLOAT], dest = next,   sendtag = 1, [psi[pm+1][1], MPI.FLOAT], source = prev, recvtag = 1, status = status)
        comm.Send([psi[1:][1:], n, MPI.FLOAT], dest = next_rank, tag = 1)
        comm.Recv([psi[pm+1:][1:], n, MPI.FLOAT], source = prev_rank, tag = 1)        
        
        # Loop over the elements computing the stream function
        for i in range(1,pm+1):
            for j in range(1,n+1):
                tmp[i][j] = 0.25 * (psi[i+1][j]+psi[i-1][j]+psi[i][j+1]+psi[i][j-1])

        # Update psi
        for i in range(1,pm+1):
            for j in range(1,n+1):
                psi[i][j] = tmp[i][j]

        # Debug output
        if iter%1000 == 0:
            sys.stdout.write("completed iteration {0}\n".format(iter))
