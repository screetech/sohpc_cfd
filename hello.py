#!/usr/bin/env python
"""
Created on Mon Jul 25 16:24:53 2016

@author: macek
"""
import sys
from mpi4py import MPI
import numpy as np

def main():
    comm    = MPI.COMM_WORLD
    my_rank = comm.Get_rank()    # my rank in the communicator
    size = comm.Get_size()       # number of processes in the communicator 
 #   status  = MPI.Status() 
    status = [MPI.Status(), MPI.Status(), MPI.Status(), MPI.Status()]  
    
   # print("Hi, I am ", my_rank, " from ", size)
    
    next_rank = (my_rank + 1) % size
    prev_rank = (my_rank - 1) % size


    buf1 = np.zeros(shape=(2, 10), dtype=np.int)    
    buf2 = np.zeros(shape=(2, 10), dtype=np.int)     
    #buf = 100    
    #data = np.array([1, 2, 3, 4, 5], dtype=np.int)     
    data = np.array([[1, 2, 3, 4, 5], [6, 7, 8, 9, 0]], dtype=np.float)
    a=5
    d    = data[0]
    #if my_rank == 0:        
    req1 = MPI.COMM_WORLD.Isend([data[0][0:2], MPI.FLOAT], dest = next_rank, tag = 0)
    req4 = MPI.COMM_WORLD.Isend([data[0][3:5], MPI.INT], dest = next_rank, tag = 1)
    print("Hi, I am " + str(my_rank) + " I am sending " + str(data[0]) + " to rank " + str(next_rank))
    req2 = MPI.COMM_WORLD.Irecv([data[1, 0:2],MPI.FLOAT], source = prev_rank, tag = 0)
    #req1.Wait()        
    #req2.Wait()
    #MPI.Request.Waitall([req1, req2], status)
    #print("Hi, I am " + str(my_rank) + " I received " + str(buf1))
    #else:
    req3 = MPI.COMM_WORLD.Irecv([data[1, 3:5],MPI.INT], source = prev_rank, tag = 1)
    
    #print("Hi, I am " + str(my_rank) + " I am sending " + str(d) + " to rank " + str(next_rank))
    #req1.Wait()        
    #req2.Wait()
    MPI.Request.Waitall([req1, req2, req3, req4], status)
    print("Hi, I am " + str(my_rank) + " I received " + str(data))
    #print("Hi, I am " + str(my_rank) + " I received " + str(buf2))
        
if __name__ == "__main__":
        main()