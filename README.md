# README #

This GIT repository contains Python code for Computational Fluid Dynamics (CFD) and Image Reconstruction.
The codes concern on parallel processing using MPI. It is focused on the usage of various types of MPI blocking and nonblocking communication, virtual topologies and rank reordering.   

### What is this repository for? ###

* This repository was created within PRACE SoHPC 2016 programme, at EPCC under my mentor Dr. Neelofer Banglawala.
* It was created on the ARCHER supercomputer (http://archer.ac.uk/).

### Environment settings ###

* Python >= 3.0, numpy, matplotlib, mpi4py, scipy (or Anaconda 3)
* MPI environment (https://jetcracker.wordpress.com/2012/03/01/how-to-install-mpi-in-ubuntu/) - needed only for running parallel version

### How to run Image Reconstruction Code ###

* Go to Image_reconstruction folder
* The input is an edge detected image in .ppm (RGB) or .pgm (grayscale) format. 
* For edge detection use pgm2edge.f90, respectively pgm2edge_gray.f90 in data folder (use fortran compiler).
* RGB image recontruction is possible only at serial version, see colour folder.
* There are developed several versions of MPI code for grayscale image.
* Run the serial code as python serial.py path_to_input_img ouput_img_name number_of_iterations
* Run the parallel code as mpirun -np number_of_processes python parallel.py path_to_input_img ouput_img_name number_of_iterations. Or follow qscript file.
* For more information see http://www.archer.ac.uk/training/course-material/2016/07/MPP_MPI_epcc/Exercises/MPP-casestudy.pdf.

### How to run CFD Code ###

* Serial version of the code is in serial folder. There are many versions of the code. 
* Parallel versions of the code are in mpi/numpy_index folder.
* Run the serial code as python cfd.py scale_factor number_of_iterations.
* Run the parallel code as mpirun -np number_of_processes python cfd.py scale_factor number_of_iterations. Or follow qscript file.
* For more information see http://www.archer.ac.uk/training/course-material/2016/07/intro_epcc/exercises/cfd.pdf.

### Notes ###
* 2D decomposition is in progress at this time (not working). 

### Blogs, reports and videos ###
* About Image Reconstruction
* * https://summerofhpc.prace-ri.eu/from-an-edge-to-the-whole-picture/
* About CFD
* * https://summerofhpc.prace-ri.eu/python-mpi-cfd-and-archer-what-is-a-bigger-challenge/
* About me
* * https://summerofhpc.prace-ri.eu/marta-cudova/
* About SoHPC at EPCC
* * https://summerofhpc.prace-ri.eu/familiarizing-with-edinburgh/
* Final video presentation
* * https://www.youtube.com/watch?v=__oNjB6DIi8&feature=youtu.be 


### Contact ###
* Marta Cudova
* screetech@gmail.com