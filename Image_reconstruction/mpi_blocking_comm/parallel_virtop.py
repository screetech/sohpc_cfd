#!/usr/bin/env python

'''
Edge detection
-------------------------------------------------
Marta Cudova, screetech@gmail.com
Simple MPI version using blocking communication and halo zones.
Using virtual topologies.

Date: 05/08/2016
'''
import numpy as np
import sys
from mpi4py import MPI

# read file, write to file, get the pic dimensions
import sharpenio as io

def main(argv):
    if len(argv) < 3:
        print("Usage: parallel.py <input_file> <output_file> <number_of_iterations>")
        sys.exit(1)

    # MPI initialisation
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()    

    
    if rank == 0:
        infile  = str(sys.argv[1])
        outfile = str(sys.argv[2])
        niter   = np.array(int(sys.argv[3]), dtype=np.int)
        # get pic dimensions
        dims = np.array(io.pgmsize(infile))
        # test dimensions to 0
        if dims[0] == 0 or dims[1] == 0:
            print("Bad picture dimensions " + dims[0] + " " + dims[1])
            MPI.Abort(1)
            sys.exit(1)
        # debug
        print("Input " + str(infile) + " output " + str(outfile) + " iterations: " + str(niter))
        print("Image size is " + str(dims[0]) + " x " + str(dims[1]))
    else:
        dims  = np.zeros((1, 2), dtype=np.int) 
        niter = np.empty(1, dtype=np.int)
    # broadcast image dimensions and the number of iterations
    comm.Bcast([dims, MPI.INT], root=0)
    comm.Bcast([niter, MPI.INT], root=0)

    if rank != 0:
        dims = dims[0]
    dx   = dims[0]
    dy   = dims[1]
    # redimension for x - slicing for each process
    px   = dx//size

    # fill arrays with white colour (255.0)
    # arrays for each process
    old       = np.full( (px+2, dy+2), 255.0, dtype=np.float)
    new       = np.full( (px+2, dy+2), 255.0, dtype=np.float)
    edge      = np.full( (px+2, dy+2), 255.0, dtype=np.float)
    buffer    = np.zeros((px,   dy),          dtype=np.float)
    masterbuf = np.zeros((dx,   dy),          dtype=np.float)

    # create virtual topology
    cart         = MPI.COMM_WORLD.Create_cart(dims=(size, 1), periods=(False, False), reorder=False) 
    left, right  = cart.Shift(direction=0, disp=1)    # displacement to right (up)
    
    tag0 = 0    # 0 - send to next rank/recv from previous one
    tag1 = 1    # 1 - send to previous rank/recv from next one

    if rank == 0:
        # read the input file
        dim2 = np.array(io.pgmread(infile, masterbuf, dx, dy))   
        # test
        if dim2[0] != dx or dim2[1] != dy:
            print("Error while reading a file.")
            sys.exit(1)
    
    comm.Barrier()
    if rank == 0:
        tstart = MPI.Wtime()
    # scatter input image from masterbuf into buffer arrays
    comm.Scatter([masterbuf, MPI.FLOAT], [buffer, MPI.FLOAT])
    # copy the buffer into edge array
    np.copyto(edge[1:px+1, 1:dy+1], buffer)

    for iter in range(niter):
        # Changing halos
        # Send to the next rank
        MPI.COMM_WORLD.Send([old[px][:], MPI.FLOAT], dest   = right, tag = tag0)
        MPI.COMM_WORLD.Recv([old[0 ][:], MPI.FLOAT], source = left,  tag = tag0) 
        # Send to the previous rank
        MPI.COMM_WORLD.Send([old[1 ][:],   MPI.FLOAT], dest   = left,  tag = tag1) 
        MPI.COMM_WORLD.Recv([old[px+1][:], MPI.FLOAT], source = right, tag = tag1) 

        new[1:px+1, 1:dy+1] = 0.25 * (old[0:px, 1:dy+1] + old[2:px+2, 1:dy+1] + old[1:px+1, 0:dy] + old[1:px+1, 2:dy+2] - edge[1:px+1, 1:dy+1]);
        # update old array with values from new array
        np.copyto(old[1:px+1, 1:dy+1], new[1:px+1, 1:dy+1])

    # copy old back to buffer excluding halos
    np.copyto(buffer, old[1:px+1, 1:dy+1])
    # gather arrays from processes to masterbuf
    comm.Gather([buffer, MPI.FLOAT], [masterbuf, MPI.FLOAT])
     
    comm.Barrier() 
    if rank == 0:
        tend = MPI.Wtime() - tstart
        print("Calculation time (scatter, loop, gather): " + str(tend) + " s") 

    if rank == 0:       
        # write to file
        io.pgmwrite(outfile, masterbuf, dx, dy)
    

# Function to create tidy way to have main method
if __name__ == "__main__":
        main(sys.argv[1:])