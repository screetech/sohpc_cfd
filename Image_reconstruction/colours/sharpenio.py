import numpy as np


def pgmsize(fname):
    f = open(fname, 'r')
    f.readline()
    f.readline()
    line = f.readline()
    cols = line.split()
    f.close()
    return (int(cols[0]),int(cols[1])) if 2 == len(cols) else (0,0)
    

def pgmread(fname, pixmap, nxmax, nymax):

    n = pgmsize(fname)
    nx = n[0]
    ny = n[1]
    if (nx > nxmax or ny > nymax):
        print("pgmread: image larger than array.")
        print("nxmax, nymax, nx, ny = ")
        print(str(nxmax), ", ", str(nymax), ", ", str(nx), ", ", str(ny), ".")
        raise SystemExit

    # get the color section 
    r = pixmap[0]
    g = pixmap[1]
    b = pixmap[2]
    f = open(fname, 'r')

    ri  = 0
    gi  = 0
    bi  = 0
    rj = (ny)-1
    gj = (ny)-1 
    bj = (ny)-1
    fline_cnt = 1

    for line in f:
        if fline_cnt > 4:
            cols = line.split()
            for k in range(len(cols)):
                if k%3 == 0:
                    r[ri][rj] = int(cols[k])
                    ri = ri+1 if ri < nx-1 else 0
                    rj = rj   if ri > 0 else rj-1
                    
                elif k%3 == 1:
                    g[gi][gj] = int(cols[k])
                    gi = gi+1 if gi < nx-1 else 0
                    gj = gj   if gi > 0 else gj-1
                else:
                    b[bi][bj] = int(cols[k])
                    bi = bi+1 if bi < nx-1 else 0
                    bj = bj   if bi > 0 else bj-1
                
        fline_cnt = fline_cnt + 1
    
    pixmap = (r, g, b)    
    f.close()

    return n
  

"""
Routine to write a PGM image file from a 2D floating point array
x[nx][ny]. Because of the way C handles (or fails to handle!)
multi-dimensional arrays we have to cast the pointer to void.
"""
def pgmwrite(fname, pixmap, nx, ny):

    thresh = float(255.0)
    PIXPERLINE = 12

    r = pixmap[0]
    g = pixmap[1]
    b = pixmap[2]

    # find the max and min absolute values of the array
    #print(r)
    rxmin = abs(r[0][0])
    rxmax = rxmin
    for i in range(nx):
        for j in range(ny):
            xabs = abs(r[i][j])
            if xabs < rxmin:
                rxmin = xabs
            elif xabs > rxmax:
                rxmax = xabs

    gxmin = abs(g[0][0])
    gxmax = gxmin
    for i in range(nx):
        for j in range(ny):
            xabs = abs(g[i][j])
            if xabs < gxmin:
                gxmin = xabs
            elif xabs > gxmax:
                gxmax = xabs
  
    bxmin = abs(b[0][0])
    bxmax = bxmin
    for i in range(nx):
        for j in range(ny):
            xabs = abs(b[i][j])
            if xabs < bxmin:
                bxmin = xabs
            elif xabs > bxmax:
                bxmax = xabs


    f = open(fname,'w')
    
    f.write("P3\n")
    f.write("# Written by pgmwrite\n")
    f.write(str(nx) + " " + str(ny) + "\n")
    f.write(str(int(thresh)) + "\n")
    
    k   = 0
    for j in range(ny-1,-1,-1):
        for i in range(nx): 
      
            rtmp = r[i][j]
            btmp = b[i][j]
            gtmp = g[i][j]

            # scale the value appropriately so it lies between 0 and thresh
            if (rxmin < 0 or rxmax > thresh):
                rtmp = int((thresh*((abs(rtmp-rxmin))/(rxmax-rxmin))) + 0.5)
            else:
                rtmp = int(abs(rtmp) + 0.5)

            # scale the value appropriately so it lies between 0 and thresh
            if (gxmin < 0 or gxmax > thresh):
                gtmp = int((thresh*((abs(gtmp-gxmin))/(gxmax-gxmin))) + 0.5)
            else:
                gtmp = int(abs(gtmp) + 0.5)    

            # scale the value appropriately so it lies between 0 and thresh
            if (bxmin < 0 or bxmax > thresh):
                btmp = int((thresh*((abs(btmp-bxmin))/(bxmax-bxmin))) + 0.5)
            else:
                btmp = int(abs(btmp) + 0.5)  
            

            # increase the contrast by boosting the lower values?
            #grey = thresh * sqrt(tmp/thresh)

            f.write(str(rtmp) + " " + str(gtmp) + " " + str(btmp) + " ")
            
            if (0 == (k+1) % PIXPERLINE):
                f.write("\n")

            k   = k + 1
    
    if (0 != k % PIXPERLINE):
        f.write("\n")
      
    f.close()



"""
Routine to write a PGM image file from a 2D floating point array
x[nx][ny]. Because of the way C handles (or fails to handle!)
multi-dimensional arrays we have to cast the pointer to void.
"""
def pgmwrite_channel(fname, pixmap, nx, ny):

    thresh = float(255.0)
    PIXPERLINE = 16

    # find the max and min absolute values of the array
    xmin = abs(pixmap[0][0])
    xmax = xmin
    for i in range(nx):
        for j in range(ny):
            xabs = abs(pixmap[i][j])
            if xabs < xmin:
                xmin = xabs
            elif xabs > xmax:
                xmax = xabs
  

    f = open(fname,'w')
    
    f.write("P2\n")
    f.write("# Written by pgmwrite\n")
    f.write(str(nx) + " " + str(ny) + "\n")
    f.write(str(int(thresh)) + "\n")
    
    k = 0

    for j in range(ny-1,-1,-1):
        for i in range(nx):

            tmp = pixmap[i][j]
      
            # scale the value appropriately so it lies between 0 and thresh
            if (xmin < 0 or xmax > thresh):
                tmp = int((thresh*((abs(tmp-xmin))/(xmax-xmin))) + 0.5)
            else:
                tmp = int(abs(tmp) + 0.5)
      
            col = tmp

            # increase the contrast by boosting the lower values?
            #grey = thresh * sqrt(tmp/thresh)

            f.write(str(col) + " ")

            if (0 == (k+1) % PIXPERLINE):
                f.write("\n")

            k = k + 1
    
    if (0 != k % PIXPERLINE):
        f.write("\n")
      
    f.close()
