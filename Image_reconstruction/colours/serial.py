#!/usr/bin/env python

'''
Edge detection
-------------------------------------------------
Marta Cudova, screetech@gmail.com
Serial version.
Date: 04/08/2016
'''
import numpy as np
np.set_printoptions(threshold=np.inf)

import sys
import time
# read file, write to file, get the pic dimensions
import sharpenio as io

def main(argv):
    if len(argv) < 3:
        print("Usage: serial.py <input_file> <output_file> <number_of_iterations>")
        sys.exit(1)

    infile  = str(sys.argv[1])
    outfile = str(sys.argv[2])
    niter   = int(sys.argv[3])
    # get pic dimensions
    dims = np.array(io.pgmsize(infile))
    dx   = dims[0]
    dy   = dims[1]
    # test dimensions to 0
    if dx == 0 or dy == 0:
        print("Bad picture dimensions " + dx + " " + dy)
        sys.exit(1)

    # debug
    print("Input " + str(infile) + " output " + str(outfile))
    print("Image size is " + str(dims[0]) + " x " + str(dims[1]))

    # fill arrays with white colour (255.0)
    # old array
    rold    = np.full((dx+2, dy+2), 255.0, dtype=np.float)
    gold    = np.full((dx+2, dy+2), 255.0, dtype=np.float)
    bold    = np.full((dx+2, dy+2), 255.0, dtype=np.float)
    # new array
    rnew    = np.full((dx+2, dy+2), 255.0, dtype=np.float)
    gnew    = np.full((dx+2, dy+2), 255.0, dtype=np.float)
    bnew    = np.full((dx+2, dy+2), 255.0, dtype=np.float)
    # edge array
    redge   = np.full((dx+2, dy+2), 255.0, dtype=np.float)
    gedge   = np.full((dx+2, dy+2), 255.0, dtype=np.float)
    bedge   = np.full((dx+2, dy+2), 255.0, dtype=np.float)
    # R, G, B array - for rgb channels from file
    r       = np.zeros((dx,  dy),          dtype=np.float)
    g       = np.zeros((dx,  dy),          dtype=np.float)
    b       = np.zeros((dx,  dy),          dtype=np.float)
    buffer  = (r, g, b) 
    # read the input file
    dim2 = np.array(io.pgmread(infile, buffer, dx, dy))
    # test
    if dim2[0] != dx or dim2[1] != dy:
        print("Error while reading a file.")
        sys.exit(1)

    tstart = time.time()
    # copy the R, G, B arrays from buffer into r-, g-, b- edge arrays
    np.copyto(redge[1:dx+1, 1:dy+1], buffer[0])
    np.copyto(gedge[1:dx+1, 1:dy+1], buffer[1])
    np.copyto(bedge[1:dx+1, 1:dy+1], buffer[2])

    for iter in range(niter):
        rnew[1:dx+1, 1:dy+1] = 0.25 * (rold[0:dx, 1:dy+1] + rold[2:dx+2, 1:dy+1] + rold[1:dx+1, 0:dy] + rold[1:dx+1, 2:dy+2] - redge[1:dx+1, 1:dy+1])
        # update old array with values from new array -  red channel
        np.copyto(rold[1:dx+1, 1:dy+1], rnew[1:dx+1, 1:dy+1])

        gnew[1:dx+1, 1:dy+1] = 0.25 * (gold[0:dx, 1:dy+1] + gold[2:dx+2, 1:dy+1] + gold[1:dx+1, 0:dy] + gold[1:dx+1, 2:dy+2] - gedge[1:dx+1, 1:dy+1])
        # update old array with values from new array - green channel
        np.copyto(gold[1:dx+1, 1:dy+1], gnew[1:dx+1, 1:dy+1])

        bnew[1:dx+1, 1:dy+1] = 0.25 * (bold[0:dx, 1:dy+1] + bold[2:dx+2, 1:dy+1] + bold[1:dx+1, 0:dy] + bold[1:dx+1, 2:dy+2] - bedge[1:dx+1, 1:dy+1])
        # update old array with values from new array - blue channel
        np.copyto(bold[1:dx+1, 1:dy+1], bnew[1:dx+1, 1:dy+1])

    tend = time.time()
    print("Calculation time (between IO, loop): " + str(tend-tstart) + "s")     
    # copy old back to buffer excluding halos
    np.copyto(r, rold[1:dx+1, 1:dy+1])
    #io.pgmwrite_channel("R_"+outfile, r, dx, dy)

    np.copyto(g, gold[1:dx+1, 1:dy+1])
    #io.pgmwrite(outfile+"G", g, dx, dy3)

    np.copyto(b, bold[1:dx+1, 1:dy+1])
    #io.pgmwrite(outfile+"B", b, dx, dy3)
    
    buffer = (r, g, b)
    # write to file
    io.pgmwrite(outfile, buffer, dx, dy)


# Function to create tidy way to have main method
if __name__ == "__main__":
        main(sys.argv[1:])

