#!/usr/bin/env python

'''
Edge detection
-------------------------------------------------
Marta Cudova, screetech@gmail.com
Simple MPI version without changing halo zones.
2D decomposition.

Date: 05/08/2016
'''
import numpy as np
np.set_printoptions(threshold=np.inf)
import sys
from mpi4py import MPI

# read file, write to file, get the pic dimensions
import sharpenio as io

def main(argv):
    if len(argv) < 3:
        print("Usage: parallel.py <input_file> <output_file> <number_of_iterations>")
        sys.exit(1)

    # MPI initialisation
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()    
    
    # temporary hack
    x = 1
    y = 1
    if size == 4:
        x = 2
        y = 2
    elif size == 8:
        x = 4
        y = 2
    elif size == 16:
        x = 4
        y = 4
    elif size == 32:
        x = 8
        y = 4
    elif size == 64:
        x = 8
        y = 8
    elif size == 128:
        x = 16
        y = 8
    elif size == 256:
        x = 16
        y = 16
    elif size == 768:
        x = 32 
        y = 24
    # create a virtual topology with reordering
    cart         = MPI.COMM_WORLD.Create_cart(dims=(x, y), periods=(False, False), reorder=True) 
    left, right  = cart.Shift(direction = 0, disp = 1) 
        
    if rank == 0:
        infile  = str(sys.argv[1])
        outfile = str(sys.argv[2])
        niter   = np.array(int(sys.argv[3]), dtype=np.int)
        # get pic dimensions
        dims = np.array(io.pgmsize(infile))
        # test dimensions to 0
        if dims[0] == 0 or dims[1] == 0:
            print("Bad picture dimensions " + dims[0] + " " + dims[1])
            MPI.Abort(1)
            sys.exit(1)
        # debug
        print("Input " + str(infile) + " output " + str(outfile) + " iterations: " + str(niter))
        print("Image size is " + str(dims[0]) + " x " + str(dims[1]))
    else:
        dims  = np.zeros((1, 2), dtype=np.int) 
        niter = np.empty(1, dtype=np.int)
    # broadcast image dimensions and the number of iterations
    comm.Bcast([dims, MPI.INT], root=0)
    comm.Bcast([niter, MPI.INT], root=0)

    if rank != 0:
        dims = dims[0]
    dx   = dims[0]
    dy   = dims[1]
    # redimension for x and y - slicing for each process
    px   = dx//(size//x)
    py   = dy//(size//y)    
    
    # for storing process coordinates in a grid
    coords = np.zeros(2) 
    # get my coordinates
    coords = cart.Get_coords(rank)
    # calculate indexes
    startx = coords[0]*px
    endx   = startx + px
    starty = coords[1]*py
    endy   = starty + py
    
    print("Rank " + str(rank) + " startx, endx " + str(startx) + ", " + str(endx) + " starty, endy " + str(starty) + ", " + str(endy) + " coords " + str(coords))
    
    # fill arrays with white colour (255.0)
    # arrays for each process
    old       = np.full( (px+2, py+2), 255.0, dtype=np.float)
    new       = np.full( (px+2, py+2), 255.0, dtype=np.float)
    edge      = np.full( (px+2, py+2), 255.0, dtype=np.float)
    buffer    = np.zeros((px,   py),          dtype=np.float)
    masterbuf = np.zeros((dx,   dy),          dtype=np.float)

    if rank == 0:
        # read the input file
        dim2 = np.array(io.pgmread(infile, masterbuf, dx, dy))   
        print(masterbuf)
        # test
        if dim2[0] != dx or dim2[1] != dy:
            print("Error while reading a file.")
            sys.exit(1)
    # ==================================================================== #
    comm.Barrier()
    if rank == 0:
        tstart = MPI.Wtime()
    
    # broadcast input image from masterbuf into buffer arrays
    comm.Bcast([masterbuf, MPI.FLOAT], root=0)
    np.copyto(buffer, masterbuf[startx:endx, starty:endy])        
    # copy the buffer into edge array
    np.copyto(edge[1:px+1, 1:py+1], buffer)
    for iter in range(niter):
        new[1:px+1, 1:py+1] = 0.25 * (old[0:px, 1:py+1] + old[2:px+2, 1:py+1] + old[1:px+1, 0:py] + old[1:px+1, 2:py+2] - edge[1:px+1, 1:py+1])
        # update old array with values from new array
        np.copyto(old[1:px+1, 1:py+1], new[1:px+1, 1:py+1])
        
    # copy old back to buffer excluding halos
    np.copyto(buffer, old[1:px+1, 1:py+1])

    # brutal force gathering
    if rank > 0:
        comm.Send([buffer,   MPI.FLOAT], dest = 0,   tag = 0) 
    else:
        np.copyto(masterbuf[startx:endx, starty:endy], buffer)
        # everybody send its chunk of data to root
        for i in range(1,size):
            comm.Recv([buffer, MPI.FLOAT], source = i,   tag = 0) 
            rcoords = cart.Get_coords(i)
            xstart  = rcoords[0]*px
            ystart  = rcoords[1]*py
            print("Rank " + str(rank) + " startx, endx " + str(xstart) + ", " + str(xstart+px) + " starty, endy " + str(ystart) + ", " + str(ystart+py) + " coords " + str(rcoords))
            np.copyto(masterbuf[xstart:xstart+px, ystart:ystart+py], buffer) 
    # ==================================================================== #        
    comm.Barrier() 
    if rank == 0:
        tend = MPI.Wtime() - tstart
        print("Calculation time (scatter, loop, gather): " + str(tend) + " s") 

    if rank == 0:       
        # write to file
        io.pgmwrite(outfile, masterbuf, dx, dy)
    

# Function to create tidy way to have main method
if __name__ == "__main__":
        main(sys.argv[1:])