#!/usr/bin/env python

'''
Edge detection
-------------------------------------------------
Marta Cudova, screetech@gmail.com
Serial version.
Date: 04/08/2016
'''
import numpy as np
import sys
import time
# read file, write to file, get the pic dimensions
import sharpenio as io

def main(argv):
    if len(argv) < 3:
        print("Usage: serial.py <input_file> <output_file> <number_of_iterations>")
        sys.exit(1)

    infile  = str(sys.argv[1])
    outfile = str(sys.argv[2])
    niter   = int(sys.argv[3])
    # get pic dimensions
    dims = np.array(io.pgmsize(infile))
    dx   = dims[0]
    dy   = dims[1]
    # test dimensions to 0
    if dx == 0 or dy == 0:
        print("Bad picture dimensions " + dx + " " + dy)
        sys.exit(1)

    # debug
    print("Input " + str(infile) + " output " + str(outfile))
    print("Image size is " + str(dims[0]) + " x " + str(dims[1]))

    # fill arrays with white colour (255.0)
    old    = np.full((dx+2, dy+2), 255.0, dtype=np.float)
    new    = np.full((dx+2, dy+2), 255.0, dtype=np.float)
    edge   = np.full((dx+2, dy+2), 255.0, dtype=np.float)
    buffer = np.zeros((dx,   dy),         dtype=np.float)

    # read the input file
    dim2 = np.array(io.pgmread(infile, buffer, dx, dy))
    
    # test
    if dim2[0] != dx or dim2[1] != dy:
        print("Error while reading a file.")
        sys.exit(1)

    tstart = time.time()
    # copy the buffer into edge array
    np.copyto(edge[1:dx+1, 1:dy+1], buffer)

    for iter in range(niter):
        new[1:dx+1, 1:dy+1] = 0.25 * (old[0:dx, 1:dy+1] + old[2:dx+2, 1:dy+1] + old[1:dx+1, 0:dy] + old[1:dx+1, 2:dy+2] - edge[1:dx+1, 1:dy+1]);
        # update old array with values from new array
        np.copyto(old[1:dx+1, 1:dy+1], new[1:dx+1, 1:dy+1])

    # copy old back to buffer excluding halos
    np.copyto(buffer[:, :], old[1:dx+1, 1:dy+1])
    
    tend = time.time()
    print("Calculation time (between IO, loop): " + str(tend-tstart) + "s")
    # write to file
    io.pgmwrite(outfile, buffer, dx, dy)


# Function to create tidy way to have main method
if __name__ == "__main__":
        main(sys.argv[1:])
