# 
# Jacobi function for CFD calculation
#
# Basic Python version using lists
#
# Modified: 20/07/2016
# Marta Cudova, screetech@gmail.com
# =========================================================================== #

import sys

#@profile
def jacobi(niter, psi):

    # Get the inner dimensions
    m = len(psi) - 2
    n = len(psi[0]) -2

    # Define the temporary array and zero it
    tmp = [[0 for col in range(n+2)] for row in range(m+2)]

    # Iterate for number of iterations
    for iter in range(1,niter+1):

        # Loop over the elements computing the stream function
        for i in range(1,m+1):
            for j in range(1,n+1):
                tmp[i][j] = 0.25 * (psi[i+1][j]+psi[i-1][j]+psi[i][j+1]+psi[i][j-1])

        # Update psi
        for i in range(1,m+1):
            for j in range(1,n+1):
                psi[i][j] = tmp[i][j]
        #print(psi)
        # Debug output
        if iter%1000 == 0:
            sys.stdout.write("completed iteration {0}\n".format(iter))
    print(psi)        

def jacobivort(niter, psi, zet, re):

    # Get the inner dimensions
    m = len(psi) - 2
    n = len(psi[0]) -2
    
    # Define the temporary arrays and zero them
    tmp_psi = [[0 for col in range(n+2)] for row in range(m+2)]
    tmp_zet = [[0 for col in range(n+2)] for row in range(m+2)]     
     
    for iter in range(1, niter+1):
        
        for i in range(1, m+1):
            for j in range(1, n+1):
                tmp_psi[i][j] = 0.25 * (psi[i+1][j] + psi[i-1][j] + psi[i][j+1] + psi[i][j-1] - zet[i][j])
                tmp_zet[i][j] = 0.25 * (zet[i+1][j] + zet[i-1][j] + zet[i][j+1] + zet[i][j-1]) - (re/16.0) * ((psi[i][j+1] - psi[i][j-1]) * (zet[i+1][j] - zet[i-1][j]) - (psi[i+1][j] - psi[i-1][j]) * (zet[i][j+1] - zet[i][j-1]))
   
        # Update psi and zet
        for i in range(1,m+1):
            for j in range(1,n+1):
                psi[i][j] = tmp_psi[i][j]
                zet[i][j] = tmp_zet[i][j]
        
        # Debug output
        if iter%1000 == 0:
            sys.stdout.write("Completed Iteration {0}\n".format(iter))        